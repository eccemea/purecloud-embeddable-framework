## Start

Run localserver on HTTPS (443) & open page https://localhost/index.html
To test it Localhost it MUST be 443 !!


https://ksearch.wordpress.com/2017/08/22/generate-and-import-a-self-signed-ssl-certificate-on-mac-osx-sierra/

LiveServer settings

   "liveServer.settings.ChromeDebuggingAttachment": false,
    "liveServer.settings.https": {
        "enable": true,
        "cert": "/Users/danielszlaski/server.crt",
        "key": "/Users/danielszlaski/server.key",
        "passphrase": "sweetPassword"
    },    
    "liveServer.settings.port": 443,
    "liveServer.settings.CustomBrowser": "chrome",