// ****
// Example and more information available at https://developer.mypurecloud.ie/api/embeddable-framework/
// ****

window.Framework = {
  config: {
    name: 'Genesys Daniel',
    clientIds: {
      'mypurecloud.ie': 'a612f520-e941-4c09-8312-ec200f8785e3', // Your OAuth ClientID
    },
    settings: {
      embedWebRTCByDefault: true,
      dedicatedLoginWindow: false,
      hideWebRTCPopUpOption: false,
      enableCallLogs: true,
      hideCallLogSubject: false,
      hideCallLogContact: false,
      hideCallLogRelation: false,
      searchTargets: ['people', 'queues', 'frameworkContacts'],
      theme: {
        primary: '#00bb00',
        text: '#DAD5DD',
      },
    },
    helpLinks: {},
    customInteractionAttributes: ['customAttribute'],
    getUserLanguage: function (callback) {
      callback('en-US');
    },
  },
  initialSetup: function () {
    window.addEventListener('message', function (event) {
      // make a call
      if (
        event.data &&
        event.data.eventName === 'clickToDial' &&
        event.data.number
      ) {
        window.PureCloud.clickToDial({
          number: event.data.number,
          autoPlace: true,
        });
      }

      // Update Status
      if (
        event.data &&
        event.data.eventName === 'updateStatusForMe' &&
        event.data.id
      ) {
        window.PureCloud.User.updateStatus({
          id: event.data.id,
        });
      }
    });
    window.PureCloud.subscribe([
      {
        type: 'UserAction',
        callback: function (category, data) {
          // Use your CRM vendor's API to create actions to be performed upon user action state changes.

          var message = {
            eventName: 'UserAction',
            body: data,
          };
          var targetOrigin = 'https://localhost';
          parent.postMessage(message, targetOrigin);
        },
      },
      {
        type: 'Notification',
        callback: function (category, data) {
          // Use your CRM vendor's API to create actions to be performed upon notification state changes.
          console.log(category);
          console.log(data);

          var message = {
            eventName: 'Notification',
            body: data,
          };
          var targetOrigin = 'https://localhost';
          parent.postMessage(message, targetOrigin);
        },
      },
    ]);
  },

  screenPop: function (searchString, interaction) {},
  processCallLog: function (
    callLog,
    interaction,
    eventName,
    onSuccess,
    onFailure
  ) {
    //https://developer.mypurecloud.ie/api/embeddable-framework/condensed-conversation-info.html
    onSuccess({
      id: '0000-0000-0000-0001',
    });

    //alert('isChat: ' + interaction.isChat);
    //alert('First customAttribute: ' + interaction.attributes['customattribute']);
  },
  openCallLog: function (callLog) {
    console.log('##: OpenCallLog');
    console.log(callLog);
  },
  contactSearch: function (searchValue, onSuccess, onFailure) {
    alert(searchValue);

    /* sample action
                action().then(resp => {
                    console.log('## success');
                }).catch(err => {
                    console.log('## failure');
                    console.log(err);   
                })
        */

    onSuccess([
      {
        type: 'external',
        name: 'Custom Contact via API',
        phone: [
          {
            number: '(317) 222-2222',
            label: 'Cell',
          },
        ],
        attributes: { example_urlpop: 'http://google.com' },
      },
    ]);
  },
};

// sample action called from a framework
function action() {
  return new Promise(function (resolve, reject) {
    const request = new XMLHttpRequest();
    const url = 'https://jsonplaceholder.typicode.com/posts';

    request.open('GET', url);
    request.send();
    request.onreadystatechange = (e) => {
      console.log(request.status);

      if (request.status != 200) {
        reject(e);
      } else {
        console.log(request.responseText);
        resolve(request.responseText);
      }
    };
  });
}
